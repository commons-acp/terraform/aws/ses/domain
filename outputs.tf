output "ses_domain_dkim" {
  value = aws_ses_domain_dkim.ses_domain_dkim.dkim_tokens
}
output "domain_name" {
  value = aws_ses_domain_identity.domain.id
}
