
resource "aws_ses_domain_identity" "domain" {
  domain = var.domain
}

resource "aws_ses_domain_dkim" "ses_domain_dkim" {
  domain = join("", aws_ses_domain_identity.domain.*.domain)
}

data "aws_iam_policy_document" "ses_sendemail_policy_document" {
  statement {
    actions   = ["ses:SendRawEmail"]
    resources = [join("", aws_ses_domain_identity.domain.*.arn)]
  }
}